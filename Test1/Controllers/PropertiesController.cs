﻿using System;
using Test1.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;

namespace Test1.Controllers
{
    public class PropertiesController : ODataController
    {
        RegisterContext db = new RegisterContext();
        [EnableQuery]
        public IQueryable<Property> Get()
        {
            return db.Properties;
        }

        [EnableQuery]
        public SingleResult<Property> Get([FromODataUri] int key)
        {
            IQueryable<Property> result = db.Properties.Where(p => p.PropertyId == key);
            return SingleResult.Create(result);
        }

        public async Task<IHttpActionResult> Post(Property property)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var prop = db.Properties.Where(p => p.Address == property.Address && p.Area == property.Area && p.OwnerId == property.OwnerId);
            if (!prop.Any())
            {
                db.Properties.Add(property);
                await db.SaveChangesAsync();
                return Created("", property);
            }
            else return BadRequest("Already exists");
        }

        [EnableQuery]
        public SingleResult<Person> GetOwner([FromODataUri] int key)
        {
            var ownerId = db.Properties.Find(key).OwnerId;
            var result = db.People.Where(p => p.PersonId == ownerId);
            return SingleResult.Create(result);
        }

        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            var property = await db.Properties.FindAsync(key);
            if (property == null) return NotFound();
            db.Properties.Remove(property);
            await db.SaveChangesAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }

        [EnableQuery]
        [AcceptVerbs("PATCH")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Property> prop)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var entity = await db.Properties.FindAsync(key);
            if (entity == null) return NotFound();
            prop.Patch(entity);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await db.Properties.FindAsync(key) == null) return NotFound();
                else throw;
            }
            return Updated(entity);
        }

        [HttpPost]
        [EnableQuery]
        public async Task<IHttpActionResult> Casc([FromODataUri] int key)
        {
            var prop = await db.Properties.FindAsync(key);
            if (prop == null) return NotFound();
            IQueryable<Property> properties = db.Properties.Where(p => p.OwnerId == prop.OwnerId);

            if(properties.Count() == 1 )
            {
                var owner = await db.People.FindAsync(prop.OwnerId);
                db.People.Remove(owner);

            }

            db.Properties.Remove(prop);
            await db.SaveChangesAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [EnableQuery]
        public async Task<IHttpActionResult> NewOwner([FromODataUri] int key, ODataActionParameters parameters)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var prop = await db.Properties.FindAsync(key);

            Person person = new Person
            {
                FirstName = (string)parameters["FirstName"],
                LastName = (string)parameters["LastName"],
                TelephoneNumber = (string)parameters["TelephoneNumber"]
            };

            var owners = db.People.Where(
            p => (person.FirstName == p.FirstName) && (person.LastName == p.LastName) && (person.TelephoneNumber == p.TelephoneNumber)
            );
            if (!owners.Any())
            {

                db.People.Add(person);
                await db.SaveChangesAsync();

                owners = db.People.Where(
                p => (person.FirstName == p.FirstName) && (person.LastName == p.LastName) && (person.TelephoneNumber == p.TelephoneNumber)
                );
            }
            person = owners.FirstOrDefault();
            prop.OwnerId = person.PersonId;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await db.People.FindAsync(person.PersonId) == null) return BadRequest();
                throw;
            }
            return Updated(prop);

        }
    }
}
