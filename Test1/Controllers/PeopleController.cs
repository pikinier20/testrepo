﻿using System;
using Test1.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.OData;

namespace Test1.Controllers
{
    public class PeopleController : ODataController
    {
        RegisterContext db = new RegisterContext();

        [EnableQuery]
        [HttpGet]
        public IQueryable<Person> Get()
        {
            return db.People;
        }

        [EnableQuery]
        [HttpGet]
        public SingleResult<Person> Get([FromODataUri] int key)
        {
            IQueryable<Person> result = db.People.Where(p => p.PersonId == key);
            return SingleResult.Create(result);
        }
        public IQueryable<Property> GetProperties([FromODataUri] int key)
        {
            IQueryable<Property> result = db.Properties.Where(p => p.OwnerId == key);
            return result;
        }
        public async Task<IHttpActionResult> Post(Person person)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.People.Add(person);
            await db.SaveChangesAsync();
            return Created("",person);
        }
        
        [EnableQuery]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Person> delta)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var person = await db.People.FindAsync(key);
            if (person == null) return NotFound();
            delta.Patch(person);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await db.People.FindAsync(key) == null) return NotFound();
                else throw;
            }
            return Updated(person);
        }


    }
}
