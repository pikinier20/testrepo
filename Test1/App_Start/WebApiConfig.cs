﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Test1.Models;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Builder;
    

namespace Test1
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<Person>("People");
            builder.EntitySet<Property>("Properties");
            var casc = builder.EntityType<Property>().Action("Casc");
            var newowner = builder.EntityType<Property>().Action("NewOwner");
            newowner.Parameter<string>("FirstName");
            newowner.Parameter<string>("LastName");
            newowner.Parameter<string>("TelephoneNumber");
            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: "odata",
                model: builder.GetEdmModel());
            
        }
    }
}
