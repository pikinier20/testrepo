﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test1.Models
{
    public class Property
    {
        public int PropertyId { get; set; }

        [Required]
        public string Address { get; set; }

        public float Area { get; set; }

        public int OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public Person Owner { get; set; }
    }
}