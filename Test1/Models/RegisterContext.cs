﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Test1.Models
{
    public class RegisterContext : DbContext
    {
        public RegisterContext() : base("name=RegisterContext")
        {
            //Database.SetInitializer<RegisterContext>(new DropCreateDatabaseIfModelChanges<RegisterContext>());
        }
        public DbSet<Person> People { get; set; }
        public DbSet<Property> Properties { get; set; }

    }
}